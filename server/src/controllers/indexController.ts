import { Request, Response } from 'express';

class IndexController {

    public index(req: Request, res: Response) {
        res.send('Buen dia');
    }
}

export const indexController = new IndexController();