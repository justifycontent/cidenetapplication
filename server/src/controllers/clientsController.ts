import { Request, Response } from 'express';

//import { createPool } from "promise-mysql";
import * as CreatePool from '../database';


class ClientsController {

    public async list(req: Request, res: Response) {
        const clients = await (await CreatePool.connect()).query('SELECT * FROM clients');       
        res.json(clients);
    }

    public async getClient(req: Request, res: Response): Promise<any> {
        const { id } = req.params;
        const client = await (await CreatePool.connect()).query('SELECT * FROM clients WHERE id = ?', [id]);
        if(client.length > 0) {
            return res.json(client[0]);
        }
        res.status(404).json({text: 'El cliente no existe'});
        
    }

    public async create(req: Request, res: Response): Promise<void> {        
        (await CreatePool.connect()).query('INSERT INTO clients set ?', [req.body]);
        res.json({message: 'Cliente guardado'});
    }

    public async delete(req: Request, res: Response) {
        const { id } = req.params;
        (await CreatePool.connect()).query('DELETE FROM clients WHERE id = ?', [id]);
        res.json('Cliente eliminado...'+req.params.id);
    }

    public async update(req: Request, res: Response): Promise<void> {   
        const { id } = req.params;
        (await CreatePool.connect()).query('UPDATE clients set ? WHERE id = ?', [req.body, id]);
        res.json('Actualizando cliente '+id);
    }
}

const clientsController = new ClientsController();
export default clientsController;