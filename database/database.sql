CREATE DATABASE nc_clients_db;

USE nc_clients_db;

CREATE TABLE clients(
    id VARCHAR(11) NOT NULL PRIMARY KEY,
    last_name VARCHAR(15) NOT NULL,
    second_last_name VARCHAR(15),
    client_name VARCHAR(15) NOT NULL,
    second_client_name VARCHAR(15),
    employment_country VARCHAR(2) NOT NULL,
    id_type VARCHAR(20),
    email VARCHAR(55) NOT NULL
);

DESCRIBE clients;